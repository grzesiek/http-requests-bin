# http-requests-bin

Trivial approach to spinning up a simple HTTP server that pretty-prints
requests in the terminal.

Written in Go.

## How to use it

Run it:

```bash
$ go run main.go
```

Make a request in a different tab:

```bash
$ curl -d '{ "name": "Yoda" }' -H "Content-Type: application/json" http://localhost:8080
```
