package main

import (
	"fmt"
	"log"
	"net/http"
	"net/http/httputil"
)

func main() {
	handler := http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		req, err := httputil.DumpRequest(r, true)
		if err != nil {
			fmt.Println("Could not dump request!")
			return
		}

		fmt.Println(string(req) + "\n")
	})

	if err := http.ListenAndServe(":8080", handler); err != nil {
		log.Fatalf("Error: %q", err)
	}
}
